#ifndef ARITHMETIQUE_H
#define ARITHMETIQUE_H
namespace arithmetique {
long int addition(int, int);
long int soustraction(int, int);
int multiplication(int, int);
int division(int, int);
}
#endif // ARITHMETIQUE_H
