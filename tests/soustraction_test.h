#ifndef SOUSTRACTION_TEST_H
#define SOUSTRACTION_TEST_H

#include <cppunit/extensions/HelperMacros.h>

// Déclaration de la classe de test
class soustractionTest : public CppUnit::TestFixture
{
  // Utilisation de la Macro permettant de définir cette classe comme
  // étant une 'fixture' : le nom fourni en paramètre permet de nommer
  // le constructeur de la classe.
  CPPUNIT_TEST_SUITE(soustractionTest);
  // Ajout des méthodes réalisant des tests
  CPPUNIT_TEST(soustraction_normale);
  CPPUNIT_TEST(soustraction_zero);
  CPPUNIT_TEST(soustraction_max);
  CPPUNIT_TEST(soustraction_min);
  // Utilisation de la Macro de fin de déclaration de la classe de
  // test
  CPPUNIT_TEST_SUITE_END();
 public:
  // Déclaration de la méthode d'initialisation (setUp) de la classe
  // de test (penser à une sorte de constructeur)
  void setUp();
  // Déclaration de la méthode de clôture (tearDown) de la classe de
  // test (penser à une sorte de déstructeur)
  void tearDown();
  // Déclaration des méthodes de test
  void soustraction_normale();
  void soustraction_zero();
  void soustraction_max();
  void soustraction_min();

private:

  
  // Déclaration des variables d'usage (optionnel)
  /* TODO: ajouter les déclarations des variables d'usage si besoin
   */

  int operandeA;
  int operandeB;
  // Déclaration des méthodes privées à usage interne aux tests
  // (optionnel)
};

#endif // SOUSTRACTION_TEST_H
